var portfolio = new Vue({
    el: '#portfolio',
    data: {
        portfolios: [
            {
                title: 'Velta Corp App',
                image: 'assets/img/portfolio/velta-app.png',
                description: 'App móvil para el control de inversiones.',
                technology: 'Ionic 5, Angular, SASS, HTML',
                actions: [
                    {
                        url: 'https://play.google.com/store/apps/details?id=com.dood.veltacorp',
                        icon: 'fab fa-google-play'
                    },
                    {
                        url: 'https://apps.apple.com/us/app/dood-velta/id1522729629',
                        icon: 'fab fa-app-store'
                    }
                ]
            },
            {
                title: 'Azell',
                image: 'assets/img/portfolio/azell.png',
                description: 'Plataforma de inversiones y cusos en línea.',
                technology: 'Laravel, MDBootstrap, JQuery, SASS, HTML, Mysql',
                actions: [
                    {
                        url: 'https://azellft.com',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'Billnex',
                image: 'assets/img/portfolio/billnex.png',
                description: 'Plataforma de inversiones y cusos en línea.',
                technology: 'Laravel, MDBootstrap, JQuery, SASS, HTML, Mysql',
                actions: [
                    {
                        url: 'https://thebillnex.com',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'Tacoo',
                image: 'assets/img/portfolio/tacoo.png',
                description: 'Plataforma web para la entrega y control de pedidos de comida en línea.',
                technology: 'Laravel, VueJS, NuxtJS, Vuetify, SASS, HTML, Mysql, Stripe',
                actions: [
                    {
                        url: 'https://tacoo.com.mx',
                        icon: 'fab fa-chrome'
                    },
                    {
                        url: 'https://demo.tacoo.com.mx',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'Taecel',
                image: 'assets/img/portfolio/taecel.png',
                description: 'App para venta de recargas electrónicas.',
                technology: 'Ionic 3, Angular, SASS, HTML',
                actions: [
                    {
                        url: 'https://play.google.com/store/apps/details?id=com.app.mobile.taecel',
                        icon: 'fab fa-google-play'
                    }
                ]
            },
            {
                title: 'Fiscal Cloud',
                image: 'assets/img/portfolio/fiscalcloud.png',
                description: 'Plataforma para facturación electrónica.',
                technology: 'Codeigniter 3, Bootstrap, CSS, HTML',
                actions: [
                    {
                        url: 'https://fiscalcloud.mx/portal/',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'Japam Móvil',
                image: 'assets/img/portfolio/japam_movil.png',
                description: 'App móvil para realizar pagos del servicio de Agua, reportes, denuncias y citas.',
                technology: 'Ionic 3, Angular, SASS, HTML, Laravel, Sr. Pago',
                actions: [
                    {
                        url: 'https://play.google.com/store/apps/details?id=com.isotech.japam_movil&hl=es_MX',
                        icon: 'fab fa-google-play'
                    },
                    {
                        url: 'https://apps.apple.com/mx/app/japam-movil/id1360683835',
                        icon: 'fab fa-app-store'
                    }
                ]
            },
            {
                title: 'Tu Ruta San Juan',
                image: 'assets/img/portfolio/tu_ruta_san_juan.png',
                description: 'App móvil que muestra las rutas cercanas a un punto de destino en San Juan del Río.',
                technology: 'Ionic 2, Angular, SASS, HTML, Google Maps',
                actions: [
                    {
                        url: 'https://play.google.com/store/apps/details?id=com.isotech.japam_movil&hl=es_MX',
                        icon: 'fab fa-google-play'
                    },
                    {
                        url: 'https://apps.apple.com/mx/app/tu-ruta-san-juan/id1271906879',
                        icon: 'fab fa-app-store'
                    }
                ]
            },
            {
                title: 'Xenón y Más',
                image: 'assets/img/portfolio/xenon.png',
                description: 'Página web y panel administrador para realizar pedidos de productos y control de inventarios.',
                technology: 'Laravel, Bootstrap, JQuery, CSS, HTML, Mysql',
                actions: [
                    {
                        url: 'http://xenonymas.com.mx/public/web/inicio',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'CUCC Prepago',
                image: 'assets/img/portfolio/prepago.png',
                description: 'App móvil para facturación electrónica CFDI 3.3.',
                technology: 'Ionic 1, AngularJS, CSS, HTML',
                actions: [
                    {
                        url: 'https://play.google.com/store/apps/details?id=com.cuccprepago.config&hl=es',
                        icon: 'fab fa-google-play'
                    },
                    {
                        url: 'https://apps.apple.com/mx/app/cucc-prepago/id1168815410',
                        icon: 'fab fa-app-store'
                    }
                ]
            },
            {
                title: 'CUCC',
                image: 'assets/img/portfolio/cucc.png',
                description: 'Sistema Web de facturación electroniva CFDI 3.3, complementos de pago, cotizaciones, adendas.',
                technology: 'Laravel, Bootstrap, JQuery, CSS, HTML, Mysql',
                actions: [
                    {
                        url: 'https://centrounicodecontrolcontable.com',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
            {
                title: 'SIROA',
                image: 'assets/img/portfolio/siroa.png',
                description: 'Sistema Web para consulta de resultados de análisis de aguas en línea.',
                technology: 'PHP, Bootstrap, JQuery, CSS, HTML, Mysql',
                actions: [
                    {
                        url: 'https://www.orozcolab.com.mx/siroa',
                        icon: 'fab fa-chrome'
                    }
                ]
            },
        ]
    }
})
